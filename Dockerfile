FROM openjdk:13-oracle

WORKDIR /app

CMD java -jar SparkGradleDockerHello.jar

EXPOSE 4567
